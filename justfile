SOURCE_REGISTRY := "docker.io/"
TARGET_REGISTRY := "registry.gitlab.com/datalyze-solutions/tools/mirror/"

@recipes:
    just --list --justfile {{justfile()}}

sync IMAGE SOURCE_REGISTRY=SOURCE_REGISTRY TARGET_REGISTRY=TARGET_REGISTRY:
    #!/usr/bin/env bash
    set -u
    SOURCE_GUN="{{SOURCE_REGISTRY}}{{IMAGE}}"
    TARGET_GUN="{{TARGET_REGISTRY}}{{IMAGE}}"

    crane copy ${SOURCE_GUN} ${TARGET_GUN}
    SOURCE_DIGEST=$(crane digest ${SOURCE_GUN})
    TARGET_DIGEST=$(crane digest ${TARGET_GUN})
    echo "SOURCE_DIGEST: ${SOURCE_DIGEST}"
    echo "TARGET_DIGEST: ${TARGET_DIGEST}"
    test ${SOURCE_DIGEST} == ${TARGET_DIGEST}

trivy-update:
    trivy image --cache-dir /tmp/trivycache/ --download-db-only --no-progress

trivy-scan IMAGE FORMAT="table" SEVERITY="CRITICAL" TARGET_REGISTRY=TARGET_REGISTRY:
    #!/usr/bin/env bash
    set -u
    TARGET_IMAGE_GUN="{{TARGET_REGISTRY}}{{IMAGE}}"
    echo "Scanning ${TARGET_IMAGE_GUN}"

    OUTPUT_ARGS="--format table"
    if [[ {{FORMAT}} = "format-gitlab" ]]; then
        curl -sSL -o /tmp/trivy-gitlab.tpl https://github.com/aquasecurity/trivy/raw/${TRIVY_VERSION:-v0.52.1}/contrib/gitlab.tpl
        OUTPUT_ARGS="--format template --template '@/tmp/trivy-gitlab.tpl' -o /tmp/gl-container-scanning-report.json"
    fi

    trivy image \
        --exit-code 0 \
        --severity {{SEVERITY}} \
        --no-progress \
        --skip-db-update \
        --cache-dir /tmp/trivycache/ \
        --ignore-unfixed \
        ${OUTPUT_ARGS} \
        ${TARGET_IMAGE_GUN}